export const COMPLETE_TODO = 'COMPLETE_TODO';

export function completeTodoAction(index, id, completed) {

    var todosRef = new Firebase('https://sometodoapp22.firebaseio.com/todo/');
    todosRef.child(id).update({
        "completed": !completed
    });

    return {
        type: COMPLETE_TODO,
        index
    }
}
