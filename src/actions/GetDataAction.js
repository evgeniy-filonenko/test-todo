import { GET_DATA } from '../constants/ActionTypes';
import Firebase from 'firebase';

export function getDataAction(data) {
    return {
        type: GET_DATA,
        data
    }
}
