import Firebase from 'firebase';
import { ADD_TODO } from '../constants/ActionTypes';

export function addTodoAction(todo) {
    const ref = new Firebase('https://sometodoapp22.firebaseio.com/');
    const todosRef = ref.child("todo");
    const newToDo = todosRef.push();
    const todoID = newToDo.key();

    newToDo.set({
        text: todo,
        completed: false
    });

    return {
        type: ADD_TODO,
        todo,
        id: todoID
    }
}
