import React, { Component } from 'react';
import TextField from 'material-ui/lib/text-field';

export default class AddTodo extends Component {
    render() {
        return (
            <div>
                <TextField
                    ref='input'
                    hintText="Write your task here and press enter"
                    onKeyDown={e => this.handleKeyDown(e)}
                /><br />
            </div>
        )
    }

    handleKeyDown(e) {
        if(e.keyCode === 13) {
            const node = this.refs.input.input;
            const text = node.value.trim();
            this.props.onAddClick(text);
            node.value = '';
        }
    }
}
