import React, { Component } from 'react'
import Todo from './Todo'
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';

import ListItem from 'material-ui/lib/lists/list-item';
import Checkbox from 'material-ui/lib/checkbox';
import List from 'material-ui/lib/lists/list';

import { getDataAction } from '../actions/GetDataAction'
import Firebase from 'firebase';

export default class TodoList extends Component {
    componentDidMount() {
        console.log('compotent mount, you can set new state there')
        const ref = new Firebase("https://sometodoapp22.firebaseio.com/");
        const todosArr = [];
        const dispatch = this.props.dispatch;

        ref.once("value", function(data) {
           const todosObj = data.val().todo;
           for (let todo in todosObj) {
               todosObj[todo].id = todo;
               todosArr.push(todosObj[todo]);
           }
           dispatch(getDataAction(todosArr))
        });
    }

    render() {
        return (
            <div>
                <List style={{
                    maxWidth: '400px'
                    }}>
                    {this.props.todos.map((todo, index) =>
                        <ListItem primaryText={todo.text}
                            key={index} leftCheckbox={<Checkbox
                            checked={todo.completed}
                            style={{
                                textDecoration: todo.completed ? 'line-through' : 'none'
                            }}
                            onClick={() => this.props.onTodoClick(index, todo.id, todo.completed)} />} />
                    )}
                </List>
            </div>
        )
    }
}
