import React, { Component } from 'react';
import { connect } from 'react-redux';
import AddTodo from './AddTodo';
import TodoList from './TodoList';

// import { completeTodoAction } from '../Actions';
import { completeTodoAction } from '../actions/completeTodoAction';
import { addTodoAction } from '../actions/AddTodoAction'

class App extends Component {

    render() {
        const { dispatch } = this.props;
        const todos = this.props.todos || [];

        return (
            <div>
                <AddTodo
                    onAddClick={text =>
                        dispatch(addTodoAction(text))
                    } />
                <TodoList
                    dispatch={dispatch}
                    todos={todos}
                    onTodoClick={(index, id, completed) =>
                        dispatch(completeTodoAction(index, id, completed))
                    } />
            </div>
        )
    }
}

function select(state) {
    console.log('state', state);
    return {
        todos: state.todos
    }
}

export default connect(select)(App)
