import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import injectTapEventPlugin from 'react-tap-event-plugin';

import { Provider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers } from 'redux';
import reducer from './reducers/Reducer';

let store = createStore(reducer);
let rootElement = document.getElementById('root');

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    rootElement
);

injectTapEventPlugin();
