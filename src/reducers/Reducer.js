//import { ADD_TODO, COMPLETE_TODO } from './Actions';
import { ADD_TODO, COMPLETE_TODO, GET_DATA } from '../constants/ActionTypes';

const initialSatte = {
    todos: [
        {
            text: 'some new task',
            completed: true,
            id: 0
        },
        {
            text: 'completed task',
            completed: true,
            id: 1
        }
    ]
};

export default function reducer (state = initialSatte, action) {
    switch (action.type) {
        case ADD_TODO:
            return Object.assign({}, state, {
                todos: [...state.todos, {
                    text: action.todo,
                    completed: false,
                    id: action.id
                }]
            });

        case COMPLETE_TODO:
            return {todos:[
                ...state.todos.slice(0, action.index),
                Object.assign({}, state.todos[action.index], {
                    completed: !state.todos[action.index].completed
                }),
                ...state.todos.slice(action.index + 1)
            ]};

        case GET_DATA:
            return Object.assign({}, state, {
                todos: action.data
            });

        default:
            return state;
    }
}
